<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{title}}</title>
    <style>
      main {
	  margin: 3em auto;
	  max-width: 1200px;
      }
      p, pre {
	  max-width: 800px;
      }

      /* Headings.  */
      h2 > a, h3 > a {
	  text-decoration: none;
	  color: black;
      }
      h2 > a:hover:after, h3 > a:hover:after {
	  content: "⁋";
	  margin-left: 0.5em;
      }

      table {
	  border-collapse: collapse;
      }

      td { border-left:   1px solid #ccc }
      tr { border-bottom: 1px solid #ccc }

      th, td { min-width: 3em }
      td { text-align: center }
      td:last-child { text-align: left }
      th { text-align: left }
      th > form { display: inline }

      /* Rotate table headings.  */
      th.rotate {
	  height: 150px;
	  white-space: nowrap;
      }

      th.rotate > div {
	  transform: translate(25px, 51px) rotate(315deg);
	  width: 30px;
      }
      th.rotate > div > span {
	  padding: 5px 10px;
      }
      /* /Rotate table headings.  */

      pre { background-color: #eee; }

      .score-good {
	  background: no-repeat left top url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTAiIGhlaWdodD0iMTAiPjxwYXRoIHN0eWxlPSJzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4IiBkPSJtIDEwLDAgLTEwLDEwIi8+PC9zdmc+Cg==);
	  background-color: lime;
      }

      .score-bad {
	  background: no-repeat right top url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTAiIGhlaWdodD0iMTAiPjxwYXRoIHN0eWxlPSJzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4IiBkPSJtIDEwLDEwIC0xMCwtMTAiLz48L3N2Zz4K);
	  background-color: red;
      }

      figure {
	  border: solid 1px black;
	  padding: 25px 50px;
	  margin: 50px;
	  max-width: 600px;
      }
      figure figcaption {
	  text-align: center;
	  margin-top: 2em;
      }

      pre, code {
	  background-color: #eee;
      }
    </style>
  </head>
  <body>
   <main>
    <h1>{{title}}</h1>
    <p>
      These are the results of running the OpenPGP interoperability
      test suite version {{version}}
      (<a href="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/commit/{{
      commit }}">{{commit}}</a>) on {{ timestamp | date(format="%Y-%m-%dT%H:%M") }}.
    </p>
    <p>
      This test suite has been <a href="#hall-of-fame">very
      successful</a> in identifying problems in many OpenPGP
      implementations.  If you want to see your implementation
      included in these results, please implement the
      <a href="https://tools.ietf.org/html/draft-dkg-openpgp-stateless-cli-01">Stateless
      OpenPGP Command Line Interface</a> and open an issue in our
      <a href="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/issues">tracker</a>.
      Note: The implementation doesn't have to be complete to be
      useful.
    </p>
    <h2>How to read the test results</h2>
    <p>
      Tests are loosely coupled in categories.  Both tests and
      categories have anchors and can be linked to.  The anchors
      should be stable enough to be included in commit messages and
      documentation.  Every test describes the setup, and may
      introduce terminology used in the test results.  Additional
      resources (e.g. certificates) required by the test can be
      inspected by clicking on the inspect button
      (<button type="submit" title="Inspect with packet dumper"
      disabled="true">🔎</button>).  The results are in tabular form.
      The producers are on the left going down, the consumers on the
      top going right.
    </p>
    <p>
      There are two kinds of tests.  In producer-consumer tests, the
      OpenPGP implementations being tested produce an artifact
      (e.g. they encrypt a message), and every implementation is used
      to consume the artifact (e.g. to decrypt the encrypted message).
      In consumer tests, the artifacts are produced by the test suite,
      and consumed by every OpenPGP implementation.  In either case,
      the artifact that is consumed can be inspected by clicking on
      the inspect button (<button type="submit" title="Inspect with
      packet dumper" disabled="true">🔎</button>) in the second column
      in every row.  If a producer failed to produce an artifact, or
      the artifact did not conform to the expectation, a cross mark
      (✗) is displayed.  Hovering over it with the mouse pointer
      reveals the error message in a tooltip.
    </p>
    <p>
      Each row now contains the result of consuming the row's artifact
      using the different OpenPGP implementations.  Here, a check mark
      (✓) indicates that the operation was successful.  The resulting
      output (e.g. the decrypted message) can be found in the tooltip.
      Like before, a cross mark (✗) indicates that the operation was
      not successful, or the produced artifact did not meet
      expectations.  Again, details can be found in the tooltip.
    </p>
    <p>
      Up to this point, we did not judge whether or not a operation
      should be successful or not, we merely recorded the facts.  This
      answers the question of how implementations react to certain
      inputs, and we can quantify that and have an informed
      conversation about the consequences.  But, we observed that the
      bare results were hard to interpret, a problem exacerbated by
      the vastness of the results due to combinatorial effects.
    </p>
    <p>
      To address this, most tests now have an expectation for the
      outcome, and an explanation for the expected outcome.  (If one
      of these expectations disagree with you, please get in touch!)
      If the result of an operation agrees with the expectation, the
      result has a green background and has a diagonal line in the
      top-left corner.  If they disagree, the background is red and
      the line is in the top-right corner.
    </p>
    <figure>
      <a name="example-test"><h3>Example test</h3></a>
      <p>
	This is an example.
      </p>
      <p>
	Additional artifacts:
	<ul>
	  <li>
	    <form>
	      <label>
		Certificate
		<button type="submit" title="Inspect with packet dumper" disabled="true">🔎</button>
	      </label>
	    </form>
	  </li>
	</ul>
      </p>
      <table>
	<tr style="border: none">
	  <th></th>
	  <th class="rotate"><div><span>Consumer</span></div></th>
	  <th class="rotate"><div><span>FooPGP/1</span></div></th>
	  <th class="rotate"><div><span>BarPGP/2</span></div></th>
	  <th class="rotate"><div><span>BazPGP/3</span></div></th>
	  <th class="rotate"><div><span>Expectation</span></div></th>
	  <th class="rotate"><div><span>Comment</span></div></th>
	</tr>
	<tr>
	  <th>Producer</th>
	  <th>Artifact</th>
	</tr>
	<tr>
	  <th>Base case</th>
	  <th>
	    <form>
	      <button type="submit" title="Inspect with packet dumper" disabled="true">🔎</button>
	    </form>
	  </th>
	  <td class="score-good">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score-good">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score-good">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score">
	    <span>✓</span>
	  </td>
	  <td class="score">
	    <span>Interoperability concern.</span>
	  </td>
	</tr>
	<tr>
	  <th>Well-formed variant</th>
	  <th>
	    <form>
	      <button type="submit" title="Inspect with packet dumper" disabled="true">🔎</button>
	    </form>
	  </th>
	  <td class="score-good">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score-good">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score-bad">
	    <span title="Error message">✗</span>
	  </td>
	  <td class="score">
	    <span>✓</span>
	  </td>
	  <td class="score">
	    <span>Interoperability concern.</span>
	  </td>
	</tr>
	<tr>
	  <th>Malformed variant</th>
	  <th>
	    <form>
	      <button type="submit" title="Inspect with packet dumper" disabled="true">🔎</button>
	    </form>
	  </th>
	  <td class="score-good">
	    <span title="Error message">✗</span>
	  </td>
	  <td class="score-bad">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score-good">
	    <span title="Error message">✗</span>
	  </td>
	  <td class="score">
	    <span>✗</span>
	  </td>
	  <td class="score">
	    <span>Message is malformed.</span>
	  </td>
	</tr>
	<tr>
	  <th>Weird variant</th>
	  <th>
	    <form>
	      <button type="submit" title="Inspect with packet dumper" disabled="true">🔎</button>
	    </form>
	  </th>
	  <td class="score">
	    <span title="Error message">✗</span>
	  </td>
	  <td class="score">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score">
	    <span title="Output of the successful operation">✓</span>
	  </td>
	  <td class="score">
	  </td>
	  <td class="score">
	  </td>
	</tr>
	<tr>
	  <th>Producer failure</th>
	  <th>
	    <span title="Producer failed">✗</span>
	  </th>
	  <td class="score">
	  </td>
	  <td class="score">
	  </td>
	  <td class="score">
	  </td>
	  <td class="score">
	    <span>✓</span>
	  </td>
	  <td class="score">
	    <span>Should work (TM).</span>
	  </td>
	</tr>
      </table>
      <figcaption>An example consumer test result</figcaption>
    </figure>
    <hr />
    <h1>Table of Contents</h1>
    <ol>
      <li>
	<a href="#test-results">Test Results</a>
	<ol>
	  {%- for section in toc -%}
	  <li><a href="#{{section.0.slug}}">{{section.0.title}}</a>
	    <ol>
	      {%- for entry in section.1 -%}
	      <li><a href="#{{entry.slug}}">{{entry.title}}</a></li>
	      {%- endfor -%}
	    </ol>
	  </li>
	  {%- endfor -%}
	</ol>
      </li>
      <li><a href="#hall-of-fame">Hall of Fame</a></li>
      <li><a href="#configuration">Configuration</a></li>
    </ol>
    <hr />
    <a name="test-results"><h1>Test Results</h1></a>
    {{body | safe}}
    <hr />
    {% include "hall-of-fame.inc.html" %}
    <hr />
    <a name="configuration"><h1>Configuration</h1></a>
    <p>
      This is the configuration used to produce this report:
    </p>
    <pre><code>{{configuration | json_encode(pretty=true)}}</code></pre>
    <hr />
    <p>
      OpenPGP interoperability test suite version {{version}}
      (<a href="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/commit/{{
		commit }}">{{commit}}</a>) powered
      by <a href="https://sequoia-pgp.org">Sequoia-PGP</a>.
    </p>
   </main>
  </body>
</html>
